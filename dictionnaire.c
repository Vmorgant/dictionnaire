#include <stdio.h>
#include <stdlib.h>

int Compare(char *chaine1, char *chaine2){
    /**
     * \fn Compare(char *chaine1, char *chaine2)
     * \brief Fonction vrifiant si le mot existe dans le dictionnaire
     * \param char *chaine1 char *chaine2
     * \return 0 si identique -1 si chaine 1 precede chaine2 -1 si chaine2 precede chaine1
     */
    int i=0;
    while( chaine1[i]!='\0' && chaine2[i]!='\0' ){
        if (chaine1[i] < chaine2[i])
            return -1;
        else if(chaine1[i] > chaine2[i])
            return 1;
        else
            i++;
    }
    if(chaine1[i]=='\0' && chaine2[i]=='\0' )
        return 0;
    else if (chaine1[i]=='\0')
        return -1;
    else
        return 1;
}
int strlen(char *mot){
    /**
     * \fn int strlen(char * mot)
     * \brief Fonction renvoyant la taille du mot
     * \param char * mot :le mot dont ont cherche la taille
     * \return la taille du mot sans '\0'
     */
    int i=0;
    while(mot[i]!='\0'){
        i++;
    }
    return i;
}
void strcpy(char *mot1,char *mot2){
     /**
     * \fn void strcpy(char *mot1,char *mot2)
     * \brief Fonction recopiant la chaine 2 dans la chaine 1
     * \param char *mot1 :la destination  char *mot1 :la source
     */
    int i=0;
    int taille=strlen(mot2);
    for(i=0;i<=taille;i++){
        mot1[i]=mot2[i];
    }
}
int Mot_existe(char *dictionnaire, char *mot){
    /**
     * \fn Mot_existe(char * dictionnaire, char * mot)
     * \brief Fonction vrifiant si le mot existe dans le dictionnaire
     * \param char * dictionnaire, char* mot :le mot que l'on recherche
     * \return 1 si le mot est prsent 0 si le mot est absent
     */

     int taille_mot,taille_mot_dico,res;
     char *chaine=NULL;
     taille_mot=strlen(mot);
     chaine=malloc(sizeof(char)*taille_mot);
     while(*dictionnaire!= '~'){
            taille_mot_dico=strlen(dictionnaire);
            if(taille_mot==taille_mot_dico){
                strcpy(chaine,dictionnaire);
                res=Compare(dictionnaire,mot);
                if (res==0){
                    return 1;
                }

            }
            dictionnaire=dictionnaire+taille_mot_dico;

     }
    return 0;

}

void Tri_tab(char **tab,int taille){
    int i=0,test;
    char *tmp=NULL;
    for(i=0;i<taille;i++){
            test=Compare(*(tab+i),*(tab+(i+1)));
        if( test == -1 ) {
            tmp = *tab+i;
            *(tab+i)=(*(tab+i+1));
            *(tab+i+1)=tmp;
            }
    }

}

int Ajout_mot(char *dictionnaire, char *mot,char ** dico,int *nb_mot){
    if( ! (Mot_existe(dictionnaire,mot))){

        *(nb_mot)=(*nb_mot)+1;
        dictionnaire=dictionnaire+(*nb_mot);
        *(dictionnaire+1)='~';
        realloc(dico,*(nb_mot)*sizeof(char*));
        dico[*nb_mot]=dictionnaire;
        strcpy(dico[*nb_mot],mot);
        Tri_tab(dico,*nb_mot);
    }
    return 0;

}

void Afficher_tab_char(char * dictionnaire,int taille){
    int i=0;
    printf("Le dictionnaire contient : \n");
    while( i<taille){
        printf("%c ",dictionnaire[i]);
        i++;
    }

}

void Save_dictionnaire(char * dictionnaire){
    int i=0;
    char tmp;
    FILE *fic=NULL;
    fic=fopen("dico","a");
    do{
        tmp=dictionnaire[i];
        fprintf(fic,"%c ",tmp);
        i++;
    }while( dictionnaire[i] != '\0' && dictionnaire[i+1] != '\0');
    fclose(fic);
}

void Lire_dictionnaire(char * dictionnaire){
    FILE *fic=NULL;
    int i=0;
    fic=fopen("dico","r");
    while( !feof(fic)){
        fscanf(fic,"%c ",&dictionnaire[i]);
        i++;
    }
    fclose(fic);
}
void Init_tab_char(char *tab,int taille){
    int i;
    for(i=0;i<taille;i++){
        tab[i]='\0';
    }

}
int main(void){
    int taille_dico=10000;
    char dictionnaire[taille_dico];
    int i;
    char *chaine=NULL;
    char **dico=NULL;
    int nb_mot=1;
    int nb_char;

    Init_tab_char(dictionnaire,taille_dico);
    nb_char=strlen(dictionnaire);
    chaine=malloc(sizeof(char)*20);
    dico=malloc(nb_mot*sizeof(char*));
    dictionnaire[0]='~';/*Un des derniers caracteres de la table ASCII*/
    dico[0]=dictionnaire;
    nb_char=strlen(dictionnaire);
    Lire_dictionnaire(dictionnaire);
    nb_char=strlen(dictionnaire);

    printf("Lecture ok\n");

    Afficher_tab_char(dictionnaire,nb_char);
    printf("\n Merci de donner un mot\n");
    scanf("%s",chaine);
    Ajout_mot(dictionnaire,chaine,dico,&nb_mot);
    printf("OK ajout\n");
    printf("%c \n ",dictionnaire[0]);
    Afficher_tab_char(dictionnaire,nb_char);
    printf("Affichage ok\n");

    Save_dictionnaire(dictionnaire);
    printf("Save ok \n");

    free(chaine);
    for(i=0;i<nb_mot;i++){
        free(dico[i]);
    }
    free(dico);
    return 0;
}
